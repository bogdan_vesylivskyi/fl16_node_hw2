const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');
const {check, validationResult} = require('express-validator');

const User = require('../../models/User');

// @route    POST api/auth/register
// @desc     Create or update user profile
// @access   Public

router.post("/register", [
        check("username").not().isEmpty(), check("password").not().isEmpty(),
    ],
    async (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({message: 'Bad request'});
        }

        const {username, password} = req.body;

        try {
            // See if user exists
            let user = await User.findOne({username});

            if (user) {
                return res.status(400).json({message: 'User already exists'});
            }

            user = new User({
                username,
                password
            });

            // Encrypt password
            const salt = await bcrypt.genSalt(10);

            user.password = await bcrypt.hash(password, salt);

            await user.save();

            res.json({message: "Success", user: {username: user.username}});
        } catch (err) {
            console.error(err.message);
            res.status(500).json({message: 'Server Error'});
        }
    });


// @route    POST api/auth/login
// @desc     Authenticate user & get token
// @access   Public

router.post('/login', [
        check("username").not().isEmpty(), check("password").not().isEmpty(),
    ],
    async (req, res) => {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
           return res.status(400).json({message: 'Bad request'});
        }

        const { username, password } = req.body;

        try {
            let user = await User.findOne({username});

            if (!user) {
                return res.status(400).json({message: 'Invalid Credentials'});
            }

            const isMatch = await bcrypt.compare(password, user.password);
            if (!isMatch) {
                return res.status(400).json({message: 'Invalid Credentials'});
            }

            const payload = {
                user: {
                    id: user.id
                }
            };

            jwt.sign(
                payload,
                config.get('jwtSecret'),
                {expiresIn: 360000},
                (err, token) => {
                    if (err) throw err;
                    res.json({message: 'Success', jwt_token: token});
                }
            );
        } catch (err) {
            console.error(err.message);
            res.status(500).json({message: 'Server Error'});
        }
    });

module.exports = router;