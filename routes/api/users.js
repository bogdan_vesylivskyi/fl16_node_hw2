const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');

const User = require('../../models/User');
const Note = require('../../models/Note');
const {check, validationResult} = require("express-validator");
const bcrypt = require("bcryptjs");

// @route    GET api/users/me
// @desc     Get current user profile
// @access   Private

router.get('/me', auth, async (req, res) => {
    try {
        const profile = await User.findById(req.user.id).select(['username', 'createdDate']);

        if (!profile) {
            return res.status(400).json({message: 'There is no profile for this user'});
        }

        res.json({user: profile});

    } catch (err) {
        console.error(err.message);
        res.status(500).json({message: 'Server Error'});
    }
});

// @route    PATCH api/users/me
// @desc     Change user's password
// @access   Private

router.patch('/me', [auth,
    [
        check('oldPassword').not().isEmpty(),
        check('newPassword').not().isEmpty(),
    ]], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({message: 'Bad request'})
    }

    const { oldPassword, newPassword } = req.body;

    try {
        const user = await User.findById(req.user.id);

        if (!user) {
            return res.status(400).json({message: 'Bad request'});
        }

        const isMatch = await bcrypt.compare(oldPassword, user.password);
        if (!isMatch) {
            return res.status(400).json({message: 'Bad request'});
        }

        // Encrypt password
        const salt = await bcrypt.genSalt(10);

        user.password = await bcrypt.hash(newPassword, salt);

        await user.save();
        res.json({ message: 'Success' });
    } catch (err) {
        console.error(err.message);
        res.status(500).json({message: 'Server Error'});
    }
});

// @route    DELETE api/users/me
// @desc     Delete user & posts
// @access   Private

router.delete('/me', auth, async (req, res) => {
    try {
        const profile = await User.findById(req.user.id);

        if(!profile) {
            return res.status(400).json({message: 'There is no profile for this user'});
        }
        // Remove user
        await User.findOneAndRemove({ _id: req.user.id });
        //Remove user posts
        await Note.deleteMany({ user: req.user.id })

        res.json({ message: 'Success' });
    } catch (err) {
        console.error(err.message);
        res.status(500).json({message: 'Server Error'});
    }
});

module.exports = router;