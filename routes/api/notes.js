const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');
const {check, validationResult} = require('express-validator');

const User = require('../../models/User');
const Note = require('../../models/Note');

// @route    POST api/notes
// @desc     Create a note
// @access   Private

router.post('/', [auth,
    [
        check('text').not().isEmpty()
    ]
], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({message: 'Bad request'})
    }

    try {
        const user = await User.findById(req.user.id).select('-password');
        const {text} = req.body;
        const newNote = new Note({
            text: text,
            name: user.username,
            user: req.user.id
        });

        await newNote.save();

        res.json({message: "Success", note: {text, user: user.username}});
    } catch (err) {
        console.error(err.message);
        res.status(500).json({message: 'Server Error'});
    }
});

// @route    GET api/notes
// @desc     Get user's notes
// @access   Private

router.get('/', auth, async (req, res) => {
    let {offset = 0, limit = 0} = req.query;

    if (offset < 0 || limit < 0) {
        return res.status(400).json({message: 'Bad request'})
    } else {
        offset = Number(offset);
        limit = Number(limit);
    }

    try {
        const notes = await Note.find({userId: req.user.id}, "-__v").skip(offset).limit(limit);

        if (!notes.length) {
            return res.status(400).json({message: 'Bad request'})
        }

        res.json({
            offset,
            limit,
            count: notes.length,
            notes
        });

    } catch (err) {
        console.error(err.message);
        res.status(500).json({message: 'Server Error'});
    }
});

// @route    GET api/notes/:id
// @desc     Get user's note by id
// @access   Private

router.get('/:id', auth, async (req, res) => {
    const {id: noteId} = req.params;

    try {
        const note = await Note.findOne({user: req.user.id, _id: noteId});

        // await Note.find({$and:[{user: req.user.id},{id: noteId}]}, function(err, user)
        if (!note) {
            return res.status(400).json({message: 'Bad request'})
        }

        res.json({ note });

    } catch (err) {
        console.error(err.message);
        res.status(500).json({message: 'Server Error'});
    }
});

// @route    PUT api/notes/:id
// @desc     Update user's note by id
// @access   Private

router.put('/:id', [auth,
    [
        check('text').not().isEmpty()
    ]
], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({message: 'Bad request'})
    }

    const {id: noteId} = req.params;
    const {text} = req.body;

    try {
        const note = await Note.findOne({user: req.user.id, _id: noteId});

        if (!note) {
            return res.status(400).json({message: 'Bad request'})
        }

        note.text = text;
        await note.save();

        res.json({message: "Success", note: {text}});
    } catch (err) {
        console.error(err.message);
        res.status(500).json({message: 'Server Error'});
    }
});

// @route    PATCH api/notes/:id
// @desc     Check/uncheck user's note by id, value for completed field should be changed to opposite
// @access   Private

router.patch('/:id', auth, async (req, res) => {
    const {id: noteId} = req.params;
    try {
        const note = await Note.findOne({user: req.user.id, _id: noteId});

        if (!note) {
            return res.status(400).json({message: 'Bad request'})
        }

        note.completed = !note.completed;
        await note.save();

        res.json({message: "Success"});
    } catch (err) {
        console.error(err.message);
        res.status(500).json({message: 'Server Error'});
    }
});

// @route    DELETE api/notes/:id
// @desc     Check/uncheck user's note by id, value for completed field should be changed to opposite
// @access   Private

router.delete('/:id', auth, async (req, res) => {
    const {id: noteId} = req.params;
    try {
        const note = await Note.findOne({user: req.user.id, _id: noteId});

        if (!note) {
            return res.status(400).json({message: 'Bad request'})
        }

        await Note.findOneAndRemove({user: req.user.id, _id: noteId});

        res.json({message: "Success"});
    } catch (err) {
        console.error(err.message);
        res.status(500).json({message: 'Server Error'});
    }
});


module.exports = router;