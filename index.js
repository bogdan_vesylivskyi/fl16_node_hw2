const fs = require("fs");
const path = require("path");
const express = require('express');
const morgan = require("morgan");
const connectDB = require("./config/db");

const loggerFile = path.join(__dirname, "logs.log");

const app = express();

const accessLogStream = fs.createWriteStream(
    path.join(__dirname, "logs.log"),
    { flags: "a" }
);
app.use(morgan("combined", { stream: accessLogStream }));

app.use(express.json());

try {
    if (!fs.existsSync(loggerFile)) {
        fs.appendFileSync(loggerFile, "", "utf8");
    }
} catch (error) {
    console.log(error);
}

// Connect Database
connectDB();

app.get('/', (req, res) => res.send('API Running'));
// Define Routes
app.use('/api/users', require('./routes/api/users'));
app.use('/api/auth', require('./routes/api/auth'));
app.use('/api/notes', require('./routes/api/notes'));

const PORT = 8080;

app.listen(PORT, () => console.log(`Server started on port: ${PORT}`));