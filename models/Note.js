const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const NoteSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'users'
    },
    username: {
        type: String,
    },
    text: {
        type: String,
        required: true
    },
    completed: {
        type: Boolean,
        default: false,
    },
    createdDate: {
        type: Date,
        default: Date.now()
    }
});

module.exports = Notes = mongoose.model('notes', NoteSchema);