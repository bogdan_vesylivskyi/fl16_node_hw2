const jwt = require('jsonwebtoken');
const config = require('config');

    module.exports = function (req, res, next) {
    // Get token from headers authorization
    let bearerHeader = req.headers.authorization;

    // Check if not token
    if (!bearerHeader) {
        return res.status(400).json({ message: 'Bad request' })
    }

    // Verify token
    try {
        const token = bearerHeader.replace("Bearer ","");
        const decoded = jwt.verify(token, config.get('jwtSecret'));

        req.user = decoded.user;
        next();
    } catch (err) {
        res.status(401).json({ message: 'Token is not valid' });
    }
}